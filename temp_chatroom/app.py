#Ref: https://github.com/bhavaniravi/rasa-site-bot
from flask import Flask
from flask import render_template,jsonify,request
import requests

application = Flask(__name__)

dm_url = "http://localhost:5005/conversations/default"

@application.route('/')
def get_html():
    return render_template('home.html')

@application.route('/chat',methods=["POST"])
def chat():
    try:
        user_message = request.form["text"]
        response = requests.post(dm_url+"/respond",json={"q":user_message})
        response = response.json()

        return jsonify(response)
    except Exception as e:
        print(e)
        return jsonify([{'recipient_id': 'default', 'text': '目前出了點問題，我沒辦法回答你的問題，抱歉'}])

@application.route('/restart',methods=["POST"])
def restart():
    response = requests.post(dm_url+"/continue",json={"events": [{"event": "restart"}]})
    return jsonify([{'recipient_id': 'default', 'text': '我已經忘光光你之前說過什麼了'}])

application.config["DEBUG"] = True
if __name__ == "__main__":
    application.run(host='0.0.0.0', port=6001)
