import sys
import subprocess
from subprocess import PIPE
import time
from custom.utils.daemon import Daemon

class ActionEndpointDaemon(Daemon):
    def run(self):
        proc = subprocess.Popen(['python', 
                                 '-m', 'rasa_core_sdk.endpoint', 
                                 '--actions', 'custom.rasa_core.actions'],
                                stdout=PIPE, stderr=PIPE, stdin=PIPE)
        time.sleep(1)
        return proc.pid
    
class CMSEndpointDaemon(Daemon):
    def run(self):
        proc = subprocess.Popen(['python', 'app.py'], cwd="./cms",
                                stdout=PIPE, stderr=PIPE, stdin=PIPE)
        time.sleep(1)
        return proc.pid
        
class RasaCoreServerDaemon(Daemon):
    def run(self):
        proc = subprocess.Popen(['python',
                                 '-m', 'rasa_core.run',
                                 '--enable_api',
                                 '--endpoints', 'conf/endpoint.yml',
                                 '-d', 'models/rasa_core/current/dialogue',
                                 '-u', 'models/rasa_nlu/default/current',
                                 '-o', 'logs/out.log'],
                                stdout=PIPE, stderr=PIPE, stdin=PIPE)
        time.sleep(1)
        return proc.pid        
        
if __name__ == "__main__":
        daemons = [
            ActionEndpointDaemon('Action Endpoint Server'),
            CMSEndpointDaemon('CMS Endpoint Server'),
            RasaCoreServerDaemon('Rasa Core Server')
        ]

        
        if len(sys.argv) == 2:
                if 'start' == sys.argv[1]:
                    for d in daemons: d.start()
                elif 'stop' == sys.argv[1]:
                    for d in daemons: d.stop()
                elif 'restart' == sys.argv[1]:
                    for d in daemons: d.restart()
                elif 'list' == sys.argv[1]:
                    sys.stdout.write("Current Daemons as follows:" + '\n')
                    for d in daemons: sys.stdout.write("* " + d.state() + '\t' +d.name + '\n')
                else:
                        print("Unknown command")
                        sys.exit(2)
                sys.exit(0)
        else:
                print("usage: %s start|stop|restart|list" % sys.argv[0])
                sys.exit(2)