from flask import Flask
from flask import jsonify, request
from flask_restplus import Api, Resource, reqparse, fields

import jsonschema
import numpy as np
import yaml
import json

from rasa_core.nlg.callback import nlg_request_format_spec

## to do using MVC
with open('./data/templates.yml') as f:
    template_dict = yaml.load(f)['templates']

app = Flask(__name__)
api = Api(app, validate=True)

rasa_core_request_body = api.model("Rasa Core Request Body",
                                 {
                                     "template": fields.String(description="just template", example="utter_greet", required=True)
                                 }
                                  )

@api.route('/nlg/bot_response')
class BotResponse(Resource):
    @api.response(200, 'Success')
    @api.response(400, 'Validation Error')
    @api.doc(body=rasa_core_request_body)
    def post(self):
                
        nlg_call = request.json
        #print(nlg_call['arguments'])
        #print(nlg_call['tracker']['slots'])
        jsonschema.validate(nlg_call, nlg_request_format_spec())
        #print(nlg_call.get("template"))
        template_list = template_dict.get(nlg_call.get("template"))
        #print(template_list)
        template_text = np.random.choice(template_list)
        text_completion_dict = nlg_call['tracker']['slots']
        try:
            text_completion_dict.update(nlg_call['arguments'])
            if text_completion_dict:
                template_text['text'] = template_text['text'].format(**text_completion_dict)

        finally:
            return jsonify(template_text)




app.config["DEBUG"] = True
if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5056)
