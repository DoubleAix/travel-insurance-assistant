# -*- coding: utf-8 -*-
import regex as re

#字符串預處理模塊，為分析器TimeNormalizer提供相應的字符串預處理服務
# 會當作未來NLP預處理模組
class StringPreHandler(object):
    
    @classmethod
    def capitalToSmall(cls, target):
        res = target.lower()
        return res


    @classmethod
    def halfToFull(cls,s):
        HALF2FULL = dict((i, i + 0xFEE0) for i in range(0x21, 0x7F))
        HALF2FULL[0x20] = 0x3000    
        '''
        Convert all ASCII characters to the full-width counterpart.
        '''
        return str(s).translate(HALF2FULL)
    
    @classmethod
    def fullToHalf(cls,s):
        FULL2HALF = dict((i + 0xFEE0, i) for i in range(0x21, 0x7F))
        FULL2HALF[0x3000] = 0x20
        '''
        Convert full-width characters to ASCII counterpart
        '''
        return str(s).translate(FULL2HALF)
    
    
#    @classmethod
#    def fullToHalf(cls, target): 
#        full_character = '０１２３４５６７８９' + \
#                         'ＡＢＣＤＥＦＧＨＩＪＫＬＭＮＯＰＱＲＳＴＵＶＷＸＹＺ' + \
#                         'ａｂｃｄｅｆｇｈｉｊｋｌｍｎｏｐｑｒｓｔｕｖｗｘｙｚ'
#        half_character = '0123456789' + \
#                         'ABCDEFGHIJKLMNOPQRSTUVWXYZ' + \
#                         'abcdefghijklmnopqrstuvwxyz'
#        res = target.translate(str.maketrans(full_character, half_character))
#        
#        return res
    
    @classmethod
    def handleOnlyDate(cls,target):
        #因為投保流程，有一個項目是要填寫投保時間區間，這很容易跟真正的日期混淆，所以客製化處理
        #可以能要把標點符號都加進去
        # 不知道有沒有更好的解決方案
        after_date = '當天|早上|上午|中午|下午|晚上|以前|之後|後|夜間|夜裡|今晚|明晚|以前|之前|前|午後|午間|早晨|早間|晨間|今早|明早|凌晨| |,|，'
        suffix_condition = '[^0-9日點時分半秒((我(的)?)?(保險|投保)?(金額))]+'
        pattern = re.compile('(?<![總]?共)((?P<date>\d+日)(?!({}))(?P<middle>{}))'.format(after_date, suffix_condition))
        res = pattern.sub('\g<date>當天\g<middle>', target)
        
    
        return res    
    
    
    @classmethod
    def delKeyword(cls, target, rules):
        """
        該方法刪除一字符串中所有匹配某一規則字串
        可用於清理一個字符串中的空白符和語氣助詞
        :param target: 待處理字符串
        :param rules: 刪除規則
        :return: 清理工作完成後的字符串
        """
        pattern = re.compile(rules)
        res = pattern.sub('', target)
        # print res
        return res


    @classmethod
    def numberTranslator(cls, target):
        """
        該方法可以將字符串中所有的用漢字表示的數字轉化為用阿拉伯數字表示的數字
        如"這裏有一千兩百個人，六百零五個來自中國"可以轉化為
        "這裏有1200個人，605個來自中國"
        此外添加支持了部分不規則表達方法
        如兩萬零六百五可轉化為20650
        兩百一十四和兩百十四都可以轉化為214
        一六零加一五八可以轉化為160+158
        該方法目前支持的正確轉化範圍是0-99999999
        該功能模塊具有良好的覆用性
        :param target: 待轉化的字符串
        :return: 轉化完畢後的字符串
        """
        
        look_ahead='下|夕|美|意|心|聖|維|福|合|賢|德|如|全|水|林|崙|重|民|峽|芝|義|灣|地|岸|星|湖|股|峰|結|腳|龜|股|堵|里|中|份|分寮|斗子|張犁|貂角|川|龍|寨溝|王子'
        look_ahead_non_ten='|多|甲'
        look_behind='斗'
        # 已增加 伍 & 佰
        pattern = re.compile("[一二兩三四五伍六七八九123456789]萬[一二兩三四五伍六七八九123456789](?!(千|佰|百|十))")
        match = pattern.finditer(target)
        for m in match:
            group = m.group()
            s = group.split("萬")
            s = [_f for _f in s if _f]
            num = 0
            if len(s) == 2:
                num += cls.wordToNumber(s[0]) * 10000 + cls.wordToNumber(s[1]) * 1000
            target = pattern.sub(str(num), target, 1)
        # 已增加 伍 & 佰
        pattern = re.compile("[一二兩三四五伍六七八九123456789]千[一二兩三四五伍六七八九123456789](?!(佰|百|十))")
        match = pattern.finditer(target)
        for m in match:
            group = m.group()
            s = group.split("千")
            s = [_f for _f in s if _f]
            num = 0
            if len(s) == 2:
                num += cls.wordToNumber(s[0]) * 1000 + cls.wordToNumber(s[1]) * 100
            target = pattern.sub(str(num), target, 1)
        # 已增加 伍
        pattern = re.compile("[一二兩三四五伍六七八九123456789]百[一二兩三四五伍六七八九123456789](?!十)")
        match = pattern.finditer(target)
        for m in match:
            group = m.group()
            s = group.split("百")
            s = [_f for _f in s if _f]
            num = 0
            if len(s) == 2:
                num += cls.wordToNumber(s[0]) * 100 + cls.wordToNumber(s[1]) * 10
            target = pattern.sub(str(num), target, 1)
        # 處理佰
        pattern = re.compile("[一二兩三四五伍六七八九123456789]佰[一二兩三四五伍六七八九123456789](?!十)")
        match = pattern.finditer(target)
        for m in match:
            group = m.group()
            s = group.split("佰")
            s = [_f for _f in s if _f]
            num = 0
            if len(s) == 2:
                num += cls.wordToNumber(s[0]) * 100 + cls.wordToNumber(s[1]) * 10
            target = pattern.sub(str(num), target, 1)

        pattern = re.compile("(?<!({}))[零一二兩三四五伍六七八九](?!({}))".format(look_behind, look_ahead+look_ahead_non_ten))
        match = pattern.finditer(target)
        for m in match:
            target = pattern.sub(str(cls.wordToNumber(m.group())), target, 1)

        pattern = re.compile("(?<=(週|周|星期))[末天日]")
        match = pattern.finditer(target)
        for m in match:
            target = pattern.sub(str(cls.wordToNumber(m.group())), target, 1)

        pattern = re.compile("(?<!(週|周|星期))0?[0-9]?十[0-9]?(?!({}))".format(look_ahead))
        match = pattern.finditer(target)
        for m in match:
            group = m.group()
            s = group.split("十")
            num = 0
            ten = cls.strToInt(s[0])
            if ten == 0:
                ten = 1
            unit = cls.strToInt(s[1])
            num = ten * 10 + unit
            target = pattern.sub(str(num), target, 1)

        pattern = re.compile("0?[1-9]百[0-9]?[0-9]?")
        match = pattern.finditer(target)
        for m in match:
            group = m.group()
            s = group.split("百")
            s = [_f for _f in s if _f]
            num = 0
            if len(s) == 1:
                hundred = int(s[0])
                num += hundred * 100
            elif len(s) == 2:
                hundred = int(s[0])
                num += hundred * 100
                num += int(s[1])
            target = pattern.sub(str(num), target, 1)
        # 處理佰
        pattern = re.compile("0?[1-9]佰[0-9]?[0-9]?")
        match = pattern.finditer(target)
        for m in match:
            group = m.group()
            s = group.split("佰")
            s = [_f for _f in s if _f]
            num = 0
            if len(s) == 1:
                hundred = int(s[0])
                num += hundred * 100
            elif len(s) == 2:
                hundred = int(s[0])
                num += hundred * 100
                num += int(s[1])
            target = pattern.sub(str(num), target, 1)

        pattern = re.compile("0?[1-9]千[0-9]?[0-9]?[0-9]?")
        match = pattern.finditer(target)
        for m in match:
            group = m.group()
            s = group.split("千")
            s = [_f for _f in s if _f]
            num = 0
            if len(s) == 1:
                thousand = int(s[0])
                num += thousand * 1000
            elif len(s) == 2:
                thousand = int(s[0])
                num += thousand * 1000
                num += int(s[1])
            target = pattern.sub(str(num), target, 1)

        pattern = re.compile("[0-9]+萬[0-9]?[0-9]?[0-9]?[0-9]?")
        match = pattern.finditer(target)
        for m in match:
            group = m.group()
            s = group.split("萬")
            s = [_f for _f in s if _f]
            num = 0
            if len(s) == 1:
                tenthousand = int(s[0])
                num += tenthousand * 10000
            elif len(s) == 2:
                tenthousand = int(s[0])
                num += tenthousand * 10000
                num += int(s[1])
            target = pattern.sub(str(num), target, 1)

        return target

    @classmethod
    def wordToNumber(cls, s):
        """
        方法numberTranslator的輔助方法，可將[零-九]正確翻譯為[0-9]
        :param s: 大寫數字
        :return: 對應的整形數，如果不是數字返回-1
        """
        if (s == '零') or (s == '0'):
            return 0
        elif (s == '一') or (s == '1'):
            return 1
        elif (s == '二') or (s == '兩') or (s == '2'):
            return 2
        elif (s == '三') or (s == '3'):
            return 3
        elif (s == '四') or (s == '4'):
            return 4
        elif (s == '五') or (s == '伍') or (s == '5'):
            return 5
        elif (s == '六') or (s == '6'):
            return 6
        elif (s == '七') or (s == '天') or (s == '日') or (s == '末') or (s == '7'):
            return 7
        elif (s == '八') or (s == '8'):
            return 8
        elif (s == '九') or (s == '9'):
            return 9
        else:
            return -1

    @classmethod
    def strToInt(cls, s):
        try:
            res = int(s)
        except:
            res = 0
        return res
