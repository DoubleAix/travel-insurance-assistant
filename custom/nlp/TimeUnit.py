# -*- coding: utf-8 -*-

import regex as re
import arrow
import copy
import json
from .TimePoint import TimePoint
from .RangeTimeEnum import RangeTimeEnum
try:
    from .LunarSolarConverter.LunarSolarConverter import *
except:
    from .LunarSolarConverter import *

from .DebugLogging import logger


# 時間語句分析
class TimeUnit(object):
    
    def __init__(self, exp_time, normalizer, contextTp):
        ## 自己新增的 因為原本程式碼邏輯有錯沒辦法正確往前推測，故加入了超多flag
        self.set_day_done = False
        self.set_hour_done = False
        self.set_minute_done = False
        self.parse_year = True
        self.parse_month = True
        self.parse_day = True
        self.parse_hour = True
        self.parse_minute = True
        self.parse_morning = False
        self.parse_currelated = False
        self.parse_spanrelated = False
        ## 判斷是否為sapn以TimeUnit為單位(後來新增的，將timespan的判斷以TimeUnit為單位)
        self.isTimeSpan =False
        self.invalidSpan = False
        self.timeSpan=''
        
        self.exp_time = exp_time
        self.normalizer = normalizer
        self.tp = TimePoint()
        self.tp_origin = contextTp
        self.isFirstTimeSolveContext = True
        self.isAllDayTime = True
        self.time = arrow.now()
        self.time_normalization()

    def time_normalization(self):
        ## 所以跟時間有關的判斷都在下面這些函式
        ## 要注意先後順序
        self.norm_setyear()
        self.norm_setmonth()
        #self.norm_setmonth_fuzzyday()
        self.norm_setday()
        self.norm_setmonth_fuzzyday()
        self.norm_setBaseRelated()
        self.norm_setCurRelated()
        self.norm_sethour()
        self.norm_setminute()
        #self.norm_setBaseRelated()
        #self.norm_setCurRelated()
        self.norm_setsecond()
        self.norm_setSpecial()
        self.norm_setSpanRelated()
        # 陰陽曆轉換有錯，故先不用
        self.norm_setHoliday()
        self.modifyTimeBase()
        self.tp_origin.tunit = copy.deepcopy(self.tp.tunit)

        # 判斷是時間點還是時間區間
        flag = True
        for i in range(0, 4):
            if self.tp.tunit[i] != -1:
                flag = False
        if flag:
            #self.normalizer.isTimeSpan = True
            ## 判斷是否為sapn以TimeUnit為單位
            self.isTimeSpan =True
        if self.isTimeSpan:
        #if self.normalizer.isTimeSpan:
            days = 0
            if self.tp.tunit[0] > 0:
                days += 365 * self.tp.tunit[0]
            if self.tp.tunit[1] > 0:
                days += 30 * self.tp.tunit[1]
            if self.tp.tunit[2] > 0:
                days += self.tp.tunit[2]
            tunit = self.tp.tunit
            for i in range(3, 6):
                if self.tp.tunit[i] < 0:
                    tunit[i] = 0
            seconds = tunit[3] * 3600 + tunit[4] * 60 + tunit[5]
            if seconds == 0 and days == 0:
                self.normalizer.invalidSpan = True
                ## 判斷是否為sapn以TimeUnit為單位
                self.invalidSpan = True
            self.normalizer.timeSpan = self.genSpan(days, seconds)
            ## 判斷是否為sapn以TimeUnit為單位
            self.timeSpan = self.genSpan(days, seconds)
            return

        time_grid = self.normalizer.timeBase.split('-')
        tunitpointer = 5
        while tunitpointer >= 0 and self.tp.tunit[tunitpointer] < 0:
            tunitpointer -= 1
        for i in range(0, tunitpointer):
            if self.tp.tunit[i] < 0:
                self.tp.tunit[i] = int(time_grid[i])
        self.time = self.genTime(self.tp.tunit)
#原本的
#     def genSpan(self, days, seconds):
#         day = seconds // (3600*24)
#         h = (seconds % (3600*24)) // 3600
#         m = ((seconds % (3600*24)) % 3600) // 60
#         s = ((seconds % (3600*24)) % 3600) % 60
#         return str(days+day) + ' days, ' + "%d:%02d:%02d" % (h, m, s)

#專案只需要天數    
    def genSpan(self, days, seconds):
        day = seconds // (3600*24)
        return str(days+day) + ' days'

    def genTime(self, tunit):
        time = arrow.get('1970-01-01 00:00:00')
        if tunit[0] > 0:
            time = time.replace(year=tunit[0])
        if tunit[1] > 0:
            time = time.replace(month=tunit[1])
        if tunit[2] > 0:
            time = time.replace(day=tunit[2])
        if tunit[3] > 0:
            time = time.replace(hour=tunit[3])
        if tunit[4] > 0:
            time = time.replace(minute=tunit[4])
        if tunit[5] > 0:
            time = time.replace(second=tunit[5])
        return time

    def norm_setyear(self):
        """
        年-規範化方法--該方法識別時間表達式單元的年字段
        :return:
        """
        # 一位數表示的年份
        rule = "(?<![0-9])[0-9]{1}(?=年)"
        pattern = re.compile(rule)
        match = pattern.search(self.exp_time)
        if match is not None:
            self.normalizer.isTimeSpan = True ## 在專案的應用場景被用到的機率很低，先忽略不處理
            year = int(match.group())
            self.tp.tunit[0] = year
        
        # 兩位數表示的年份
        # 可能要額外處理口語化的方式(EX:17年表示2017年 or 08年表示2008年)(the fuction modifyTimeBase handles this issue)
        rule = "[0-9]{2}(?=年)"
        pattern = re.compile(rule)
        match = pattern.search(self.exp_time)
        if match is not None:
            year = int(match.group())
            self.tp.tunit[0] = year
        
#        # 三位數表示的年份
#         rule = "(?<![0-9])[0-9]{3}(?=年)"
#         pattern = re.compile(rule)
#         match = pattern.search(self.exp_time)
#         if match is not None:
#             self.normalizer.isTimeSpan = True
#             year = int(match.group())
#             self.tp.tunit[0] = year

        ## 三位數表示的年份(民國年判斷)(範圍限定在民國100年至民國199年)
        rule = "(?<![0-9])[1]\\d\\d(?=年)"
        pattern = re.compile(rule)
        match = pattern.search(self.exp_time)
        if match is not None:
            year = int(match.group())
            year = 1911+year
            self.tp.tunit[0] = year

      
        # 四位數表示的年份
        rule = "[0-9]{4}(?=年)"
        pattern = re.compile(rule)
        match = pattern.search(self.exp_time)
        if match is not None:
            year = int(match.group())
            self.tp.tunit[0] = year

            
        ## 民國年份(範圍限定在民國1年至民國199年)
        rule = "(?<=民國)([1-9]\\d{0,1}|(?<![0-9])[1]\\d\\d)(?=年)"
        pattern = re.compile(rule)
        match = pattern.search(self.exp_time)
        if match is not None:
            year = int(match.group())
            year = 1911+year
            self.tp.tunit[0] = year
        
        if self.tp.tunit[0] == -1:
            logger.debug("year is not parsed.")
            self.parse_year = False
        else:
            logger.debug("year is parsed.")
            self.parse_year = True

    def norm_setmonth(self):
        """
        月-規範化方法--該方法識別時間表達式單元的月字段
        :return:
        """
        rule = "((10)|(11)|(12)|([1-9]))(?=月)"
        pattern = re.compile(rule)
        match = pattern.search(self.exp_time)
        if match is not None:
            self.tp.tunit[1] = int(match.group())
            #print("notfunzzy" + str(self.tp.tunit[1]))
            # 處理傾向於未來時間的情況
            self.preferFuture(1)
        if self.tp.tunit[1] == -1:
            logger.debug("month is not parsed.") 
            self.parse_month = False        
        else:
            logger.debug("month is parsed.")
            self.parse_month = True

    def norm_setmonth_fuzzyday(self):
        """
        月-日 兼容模糊寫法：該方法識別時間表達式單元的月、日字段
        :return:
        """
        rule = "((10)|(11)|(12)|([1-9]))(月|\\.|\\-)([0-3][0-9]|[1-9])"
        pattern = re.compile(rule)
        match = pattern.search(self.exp_time)
        if match is not None:
            matchStr = match.group()
            p = re.compile("(月|\\.|\\-)")
            m = p.search(matchStr)
            if match is not None:
                splitIndex = m.start()
                month = matchStr[0: splitIndex]
                day = matchStr[splitIndex + 1:]
                self.tp.tunit[1] = int(month)
                self.tp.tunit[2] = int(day)
                
                
                # 處理傾向於未來時間的情況
                self.preferFuture(1)
        if self.tp.tunit[1] == -1:
            logger.debug("month is not parsed.") 
            self.parse_month = False        
        else:
            logger.debug("month is parsed.")
            self.parse_month = True
        if self.tp.tunit[2] == -1:
            logger.debug("day is not parsed.") 
            self.parse_day = False        
        else:
            logger.debug("day is parsed.")
            self.parse_day = True

    def norm_setday(self):
        """
        日-規範化方法：該方法識別時間表達式單元的日字段
        :return:
        """
        rule = "((?<!\\d))([0-3][0-9]|[1-9])(?=(日|號))"
        pattern = re.compile(rule)
        match = pattern.search(self.exp_time)
        if match is not None:
            self.tp.tunit[2] = int(match.group())
            # 處理傾向於未來時間的情況
            self.preferFuture(2)            
        if self.tp.tunit[2] == -1:
            logger.debug("day is not parsed.") 
            self.parse_day = False        
        else:
            logger.debug("day is parsed.")
            self.parse_day = True
        self.set_day_done == True

    def norm_sethour(self):
        """
        時-規範化方法：該方法識別時間表達式單元的時字段
        :return:
        """
        rule = "(?<!(週|周|星期|禮拜))([0-2]?[0-9])(?=(點|時))"
        pattern = re.compile(rule)
        match = pattern.search(self.exp_time)
        if match is not None:
            self.tp.tunit[3] = int(match.group())
            # 處理傾向於未來時間的情況
            self.preferFuture(3)
            self.isAllDayTime = False
        if self.tp.tunit[3] == -1:
            logger.debug("hour is not parsed.") 
            self.parse_hour = False
        else:
            logger.debug("hour is parsed.")
            self.parse_hour = True

        # * 對關鍵字：早（包含早上/早晨/早間），上午，中午,午間,下午,午後,晚上,傍晚,晚間,晚,pm,PM的正確時間計算
        # * 規約：
        # * 1.中午/午間0-10點視為12-22點
        # * 2.下午/午後0-11點視為12-23點
        # * 3.晚上/傍晚/晚間/晚1-11點視為13-23點，12點視為0點
        # * 4.0-11點pm/PM視為12-23點
        rule = "凌晨"
        pattern = re.compile(rule)
        match = pattern.search(self.exp_time)
        if match is not None:
            if self.tp.tunit[3] == -1: # 增加对没有明确时间点，只写了“凌晨”这种情况的处理
                self.tp.tunit[3] = RangeTimeEnum.day_break
                # 處理傾向於未來時間的情況
                self.preferFuture(3)
                self.isAllDayTime = False

        rule = "早上|早晨|早間|晨間|今早|明早"
        pattern = re.compile(rule)
        match = pattern.search(self.exp_time)
        if match is not None:
            self.parse_morning = True
            if self.tp.tunit[3] == -1:  # 增加對沒有明確時間點，只寫了“早上/早晨/早間”這種情況的處理
                self.tp.tunit[3] = RangeTimeEnum.early_morning
                # 處理傾向於未來時間的情況
                self.preferFuture(3)
                self.isAllDayTime = False
                
        rule = "上午"
        pattern = re.compile(rule)
        match = pattern.search(self.exp_time)
        if match is not None:
            self.parse_morning = True
            if self.tp.tunit[3] == -1:  # 增加對沒有明確時間點，只寫了“上午”這種情況的處理
                self.tp.tunit[3] = RangeTimeEnum.morning
                # 處理傾向於未來時間的情況
                self.preferFuture(3)
                self.isAllDayTime = False
                

        rule = "(中午)|(午間)"
        pattern = re.compile(rule)
        match = pattern.search(self.exp_time)
        if match is not None:
            if 0 <= self.tp.tunit[3] <= 10:
                self.tp.tunit[3] += 12
            if self.tp.tunit[3] == -1:  # 增加對沒有明確時間點，只寫了“中午/午間”這種情況的處理
                self.tp.tunit[3] = RangeTimeEnum.noon
            # 處理傾向於未來時間的情況
            self.preferFuture(3)
            self.isAllDayTime = False

        rule = "(下午)|(午後)|(pm)|(PM)"
        pattern = re.compile(rule)
        match = pattern.search(self.exp_time)
        if match is not None:
            if 0 <= self.tp.tunit[3] <= 11:
                self.tp.tunit[3] += 12
            if self.tp.tunit[3] == -1:  # 增加對沒有明確時間點，只寫了“下午|午後”這種情況的處理
                self.tp.tunit[3] = RangeTimeEnum.afternoon
            # 處理傾向於未來時間的情況
            self.preferFuture(3)
            self.isAllDayTime = False

        rule = "晚上|夜間|夜裡|今晚|明晚"
        pattern = re.compile(rule)
        match = pattern.search(self.exp_time)
        if match is not None:
            if 0 <= self.tp.tunit[3] <= 10:
                self.tp.tunit[3] += 12
            elif self.tp.tunit[3] == 12:
                self.tp.tunit[3] = 0
            elif self.tp.tunit[3] == -1:  # 增加對沒有明確時間點，只寫了“下午|午後”這種情況的處理
                self.tp.tunit[3] = RangeTimeEnum.lateNight
            # 處理傾向於未來時間的情況
            self.preferFuture(3)
            self.isAllDayTime = False
        self.set_hour_done == True

    def norm_setminute(self):
        """
        分-規範化方法：該方法識別時間表達式單元的分字段
        :return:
        """
        rule = "([0-9]+(?=分(?!鐘)))|((?<=((?<!小)[點時]))[0-5]?[0-9](?!刻))"
        pattern = re.compile(rule)
        match = pattern.search(self.exp_time)
        if match is not None:
            if match.group() != '':
                self.tp.tunit[4] = int(match.group())
                # 處理傾向於未來時間的情況
                self.preferFuture(4)
                self.isAllDayTime = False
        if self.tp.tunit[4] == -1:
            logger.debug("minute is not parsed.") 
            self.parse_minute = False
        else:
            logger.debug("minute is parsed.")
            self.parse_minute = True
        # 加對一刻，半，3刻的正確識別（1刻為15分，半為30分，3刻為45分）
        rule = "(?<=[點時])[1一]刻(?!鐘)"
        pattern = re.compile(rule)
        match = pattern.search(self.exp_time)
        if match is not None:
            self.tp.tunit[4] = 15
            # 處理傾向於未來時間的情況
            # self.preferFuture(4)
            self.isAllDayTime = False

        rule = "(?<=[點時])半"
        pattern = re.compile(rule)
        match = pattern.search(self.exp_time)
        
        if match is not None:
            logger.debug("convert '半' to 30 minutes")
            self.tp.tunit[4] = 30
            # 處理傾向於未來時間的情況
            self.preferFuture(4)
            self.isAllDayTime = False

        rule = "(?<=[點時])[3三]刻(?!鐘)"
        pattern = re.compile(rule)
        match = pattern.search(self.exp_time)
        if match is not None:
            self.tp.tunit[4] = 45
            # 處理傾向於未來時間的情況
            # self.preferFuture(4)
            self.isAllDayTime = False
        self.set_minute_done = True

    def norm_setsecond(self):
        """
        添加了省略“秒”說法的時間：如17點15分32
        :return:
        """
        rule = "([0-9]+(?=秒))|((?<=分)[0-5]?[0-9])"
        pattern = re.compile(rule)
        match = pattern.search(self.exp_time)
        if match is not None:
            self.tp.tunit[5] = int(match.group())
            self.isAllDayTime = False

    def norm_setSpecial(self):
        """
        特殊形式的規範化方法-該方法識別特殊形式的時間表達式單元的各個字段
        :return:
        """
        rule = "(?<!(週|周|星期|禮拜))([0-2]?[0-9]):[0-5]?[0-9]:[0-5]?[0-9]"
        pattern = re.compile(rule)
        match = pattern.search(self.exp_time)
        if match is not None:
            tmp_target = match.group()
            tmp_parser = tmp_target.split(":")
            self.tp.tunit[3] = int(tmp_parser[0])
            self.tp.tunit[4] = int(tmp_parser[1])
            self.tp.tunit[5] = int(tmp_parser[2])
            # 處理傾向於未來時間的情況
            self.preferFuture(3)
            self.isAllDayTime = False
        else:
            rule = "(?<!(週|周|星期|禮拜))([0-2]?[0-9]):[0-5]?[0-9]"
            pattern = re.compile(rule)
            match = pattern.search(self.exp_time)
            if match is not None:
                tmp_target = match.group()
                tmp_parser = tmp_target.split(":")
                self.tp.tunit[3] = int(tmp_parser[0])
                self.tp.tunit[4] = int(tmp_parser[1])
                # 處理傾向於未來時間的情況
                self.preferFuture(3)
                self.isAllDayTime = False

        rule = "[0-9]?[0-9]?[0-9]{2}-((10)|(11)|(12)|([1-9]))-((?<!\\d))([0-3][0-9]|[1-9])"
        pattern = re.compile(rule)
        match = pattern.search(self.exp_time)
        if match is not None:
            tmp_target = match.group()
            tmp_parser = tmp_target.split("-")
            self.tp.tunit[0] = int(tmp_parser[0])
            self.tp.tunit[1] = int(tmp_parser[1])
            self.tp.tunit[2] = int(tmp_parser[2])

        rule = "((10)|(11)|(12)|([1-9]))/((?<!\\d))([0-3][0-9]|[1-9])/[0-9]?[0-9]?[0-9]{2}"
        pattern = re.compile(rule)
        match = pattern.search(self.exp_time)
        if match is not None:
            tmp_target = match.group()
            tmp_parser = tmp_target.split("/")
            self.tp.tunit[1] = int(tmp_parser[0])
            self.tp.tunit[2] = int(tmp_parser[1])
            self.tp.tunit[0] = int(tmp_parser[2])

        rule = "[0-9]?[0-9]?[0-9]{2}\\.((10)|(11)|(12)|([1-9]))\\.((?<!\\d))([0-3][0-9]|[1-9])"
        pattern = re.compile(rule)
        match = pattern.search(self.exp_time)
        if match is not None:
            tmp_target = match.group()
            tmp_parser = tmp_target.split("\\.")
            self.tp.tunit[0] = int(tmp_parser[0])
            self.tp.tunit[1] = int(tmp_parser[1])
            self.tp.tunit[2] = int(tmp_parser[2])

    def norm_setBaseRelated(self):
        """
        設置以上文時間為基準的時間偏移計算
        :return:
        """
        cur = arrow.get(self.normalizer.timeBase, "YYYY-M-D-H-m-s")
        flag = [False, False, False]

        rule = "\\d+(?=天[以之]?前)"
        pattern = re.compile(rule)
        match = pattern.search(self.exp_time)
        if match is not None:
            flag[2] = True
            day = int(match.group())
            cur = cur.shift(days=-day)

        rule = "\\d+(?=天[以之]?後)"
        pattern = re.compile(rule)
        match = pattern.search(self.exp_time)
        if match is not None:
            flag[2] = True
            day = int(match.group())
            cur = cur.shift(days=day)

        rule = "\\d+(?=(個)?月[以之]?前)"
        pattern = re.compile(rule)
        match = pattern.search(self.exp_time)
        if match is not None:
            flag[1] = True
            month = int(match.group())
            cur = cur.shift(months=-month)

        rule = "\\d+(?=(個)?月[以之]?後)"
        pattern = re.compile(rule)
        match = pattern.search(self.exp_time)
        if match is not None:
            flag[1] = True
            month = int(match.group())
            cur = cur.shift(months=month)

        rule = "\\d+(?=年[以之]?前)"
        pattern = re.compile(rule)
        match = pattern.search(self.exp_time)
        if match is not None:
            flag[0] = True
            year = int(match.group())
            cur = cur.shift(years=-year)

        rule = "\\d+(?=年[以之]?後)"
        pattern = re.compile(rule)
        match = pattern.search(self.exp_time)
        if match is not None:
            flag[0] = True
            year = int(match.group())
            cur = cur.shift(years=year)

        if flag[0] or flag[1] or flag[2]:
            self.tp.tunit[0] = int(cur.year)
        if flag[1] or flag[2]:
            self.tp.tunit[1] = int(cur.month)
        if flag[2]:
            self.tp.tunit[2] = int(cur.day)
            self.parse_spanrelated = True

    # TODO 時間長度相關
    def norm_setSpanRelated(self):
        """
        設置時間長度相關的時間表達式
        :return:
        """
        rule = "\\d+(?=個月(?![以之]?[前後]))"
        pattern = re.compile(rule)
        match = pattern.search(self.exp_time)
        if match is not None:
            self.normalizer.isTimeSpan = True
            month = int(match.group())
            self.tp.tunit[1] = int(month)

        rule = "\\d+(?=天(?![以之]?[前後]))"
        pattern = re.compile(rule)
        match = pattern.search(self.exp_time)
        if match is not None:
            self.normalizer.isTimeSpan = True
            self.isTimeSpan = True
            day = int(match.group())
            self.tp.tunit[2] = int(day)

        rule = "\\d+(?=(個)?小時(?![以之]?[前後]))"
        pattern = re.compile(rule)
        match = pattern.search(self.exp_time)
        if match is not None:
            self.normalizer.isTimeSpan = True
            hour = int(match.group())
            self.tp.tunit[3] = int(hour)

        rule = "\\d+(?=分鐘(?![以之]?[前後]))"
        pattern = re.compile(rule)
        match = pattern.search(self.exp_time)
        if match is not None:
            self.normalizer.isTimeSpan = True
            minute = int(match.group())
            self.tp.tunit[4] = int(minute)

        rule = "\\d+(?=秒鐘(?![以之]?[前後]))"
        pattern = re.compile(rule)
        match = pattern.search(self.exp_time)
        if match is not None:
            self.normalizer.isTimeSpan = True
            second = int(match.group())
            self.tp.tunit[5] = int(second)

        rule = "\\d+(?=(個)?(週|周|星期|禮拜)(?![以之]?[前後]))"
        pattern = re.compile(rule)
        match = pattern.search(self.exp_time)
        if match is not None:
            self.normalizer.isTimeSpan = True
            self.isTimeSpan = True
            week = int(match.group())
            if self.tp.tunit[2] == -1:
                self.tp.tunit[2] = 0
            self.tp.tunit[2] += int(week*7)

    # todo 節假日相關
    # 這裡有寫錯，上面先comment掉 陰曆轉陽曆的部份
    def norm_setHoliday(self):
        rule = "(清明)|(青年節)|(教師節)|(中元節)|(端午)|(勞動節)|(7夕)|(七夕)|((白色)?情人節)|(愚人節)|(中和節)|(聖誕)|(中秋)|(春節)|(元宵)|(父親節)|(兒童節)|(國慶)|(植樹節)|(元旦)|(重陽節)|(婦女節)|(萬聖節)"
        pattern = re.compile(rule)
        match = pattern.search(self.exp_time)
        if match is not None:
            if self.tp.tunit[0] == -1:
                self.tp.tunit[0] = int(self.normalizer.timeBase.split('-')[0])
            holi = match.group()
            if '節' not in holi:
                holi += '節'
            if holi in self.normalizer.holi_solar:
                date = self.normalizer.holi_solar[holi].split('-')
            if holi in self.normalizer.holi_lunar:
                date = self.normalizer.holi_lunar[holi].split('-')
                
                lsConverter = LunarSolarConverter()
                lunar = Lunar(self.tp.tunit[0], int(date[0]), int(date[1]), False)
                solar = lsConverter.LunarToSolar(lunar)
                self.tp.tunit[0] = solar.solarYear
                date[0] = solar.solarMonth
                date[1] = solar.solarDay
            self.tp.tunit[1] = int(date[0])
            self.tp.tunit[2] = int(date[1])


    def norm_setCurRelated(self):
        """
        設置當前時間相關的時間表達式
        :return:
        """
        cur = arrow.get(self.normalizer.timeBase, "YYYY-M-D-H-m-s")
        flag = [False, False, False]

        rule = "前年"
        pattern = re.compile(rule)
        match = pattern.search(self.exp_time)
        if match is not None:
            flag[0] = True
            cur = cur.shift(years=-2)

        rule = "去年"
        pattern = re.compile(rule)
        match = pattern.search(self.exp_time)
        if match is not None:
            flag[0] = True
            cur = cur.shift(years=-1)

        rule = "今年"
        pattern = re.compile(rule)
        match = pattern.search(self.exp_time)
        if match is not None:
            flag[0] = True
            cur = cur.shift(years=0)

        rule = "明年"
        pattern = re.compile(rule)
        match = pattern.search(self.exp_time)
        if match is not None:
            flag[0] = True
            cur = cur.shift(years=1)

        rule = "後年"
        pattern = re.compile(rule)
        match = pattern.search(self.exp_time)
        if match is not None:
            flag[0] = True
            cur = cur.shift(years=2)

        rule = "上(個)?月"
        pattern = re.compile(rule)
        match = pattern.search(self.exp_time)
        if match is not None:
            flag[1] = True
            cur = cur.shift(months=-1)

        rule = "(本|這個)月"
        pattern = re.compile(rule)
        match = pattern.search(self.exp_time)
        if match is not None:
            flag[1] = True
            cur = cur.shift(months=0)

        rule = "下(個)?月"
        pattern = re.compile(rule)
        match = pattern.search(self.exp_time)
        if match is not None:
            flag[1] = True
            cur = cur.shift(months=1)

        rule = "大前天"
        pattern = re.compile(rule)
        match = pattern.search(self.exp_time)
        if match is not None:
            flag[2] = True
            cur = cur.shift(days=-3)

        rule = "(?<!大)前天"
        pattern = re.compile(rule)
        match = pattern.search(self.exp_time)
        if match is not None:
            flag[2] = True
            cur = cur.shift(days=-2)

        rule = "昨"
        pattern = re.compile(rule)
        match = pattern.search(self.exp_time)
        if match is not None:
            flag[2] = True
            cur = cur.shift(days=-1)

        rule = "今(?!年)"
        pattern = re.compile(rule)
        match = pattern.search(self.exp_time)
        if match is not None:
            flag[2] = True
            cur = cur.shift(days=0)

        rule = "明(?!年)"
        pattern = re.compile(rule)
        match = pattern.search(self.exp_time)
        if match is not None:
            flag[2] = True
            cur = cur.shift(days=1)
          

        rule = "(?<!大)後天"
        pattern = re.compile(rule)
        match = pattern.search(self.exp_time)
        if match is not None:
            logger.debug("後天 is parsed")
            flag[2] = True
            cur = cur.shift(days=2)
           
        rule = "大後天"
        pattern = re.compile(rule)
        match = pattern.search(self.exp_time)
        if match is not None:
            flag[2] = True
            cur = cur.shift(days=3)
          

        # todo 補充星期相關的預測 done
        rule = "(?<=(上上(週|周|星期|禮拜)))[1-7]?"
        pattern = re.compile(rule)
        match = pattern.search(self.exp_time)
        if match is not None:
            flag[2] = True
            try:
                week = int(match.group())
            except:
                week = 1
            week -= 1
            span = week - cur.weekday()
            cur = cur.replace(weeks=-2, days=span)

        rule = "(?<=((?<!上)上(週|周|星期|禮拜)))[1-7]?"
        pattern = re.compile(rule)
        match = pattern.search(self.exp_time)
        if match is not None:
            flag[2] = True
            try:
                week = int(match.group())
            except:
                week = 1
            week -= 1
            span = week - cur.weekday()
            cur = cur.replace(weeks=-1, days=span)

        rule = "(?<=((?<!下)下(週|周|星期|禮拜)))[1-7]?"
        pattern = re.compile(rule)
        match = pattern.search(self.exp_time)
        if match is not None:
            flag[2] = True
            try:
                week = int(match.group())
            except:
                week = 1
            week -= 1
            span = week - cur.weekday()
            cur = cur.replace(weeks=1, days=span)

        rule = "(?<=(下下(週|周|星期|禮拜)))[1-7]?"
        pattern = re.compile(rule)
        match = pattern.search(self.exp_time)
        if match is not None:
            flag[2] = True
            try:
                week = int(match.group())
            except:
                week = 1
            week -= 1
            span = week - cur.weekday()
            cur = cur.replace(weeks=2, days=span)

        rule = "(?<=((?<!(上|下|個|[0-9]))(週|周|星期|禮拜)))[1-7]"
        pattern = re.compile(rule)
        match = pattern.search(self.exp_time)
        if match is not None:
            flag[2] = True
            try:
                week = int(match.group())
            except:
                week = 1
            week -= 1
            span = week - cur.weekday()
            cur = cur.replace(days=span)
           # 處理未來時間
            cur = self.preferFutureWeek(week, cur)

        if flag[0] or flag[1] or flag[2]:
            self.tp.tunit[0] = int(cur.year)
        if flag[1] or flag[2]:
            self.tp.tunit[1] = int(cur.month)
        if flag[2]:
            self.tp.tunit[2] = int(cur.day)
            self.parse_currelated = True

    def modifyTimeBase(self):
        """
        該方法用於更新timeBase使之具有上下文關聯性
        :return:
        """
        if not self.normalizer.isTimeSpan:
            if 30 <= self.tp.tunit[0] < 100:
                self.tp.tunit[0] = 1900 + self.tp.tunit[0]
            if 0 < self.tp.tunit[0] < 30:
                self.tp.tunit[0] = 2000 + self.tp.tunit[0]
            time_grid = self.normalizer.timeBase.split('-')
            arr = []
            for i in range(0, 6):
                if self.tp.tunit[i] == -1:
                    arr.append(str(time_grid[i]))
                else:
                    arr.append(str(self.tp.tunit[i]))
            self.normalizer.timeBase = '-'.join(arr)

    def preferFutureWeek(self, weekday, cur):
        # 1. 確認用戶選項
        if not self.normalizer.isPreferFuture:
            return cur
        # 2. 檢查被檢查的時間級別之前，是否沒有更高級的已經確定的時間，如果有，則不進行處理
        for i in range(0, 2):
            if self.tp.tunit[i] != -1:
                return cur
        # 獲取當前是在周幾，如果識別到的時間小於當前時間，則識別時間為下一周
        tmp = arrow.get(self.normalizer.timeBase, "YYYY-M-D-H-m-s")
        curWeekday = tmp.weekday()
        if curWeekday > weekday:
            cur = cur.shift(days=7)
        return cur

    def preferFuture(self, checkTimeIndex):
        """
        如果用戶選項是傾向於未來時間，檢查checkTimeIndex所指的時間是否是過去的時間，如果是的話，將大一級的時間設為當前時間的+1。
        如在晚上說“早上8點看書”，則識別為明天早上;
        12月31日說“3號買菜”，則識別為明年1月的3號。
        :param checkTimeIndex: _tp.tunit時間數組的下標
        :return:
        """
        time_arr = self.normalizer.timeBase.split('-')
        cur = arrow.get(self.normalizer.timeBase, "YYYY-M-D-H-m-s")
        cur_unit = int(time_arr[checkTimeIndex])
       
         # 自行新增
        """
        self.parse_year: 是否有parse到年份
        self.parse_month: 是否有parse到月份
        self.parse_day: 是否有parse到日期
        self.set_minute_done: 確保以下程式碼只執行到一次
        self.parse_currelated: 確定是否使用者有宣告到改變日的字詞(ex:明天、後天)
        self.parse_spanrelated: 確定是否使用者有宣告到改變日的字詞(ex:5天之後)
        """
        cur_unit_month = int(time_arr[1])
        cur_unit_day = int(time_arr[2])
        cur_unit_hour = int(time_arr[3])
        cur_unit_minute = int(time_arr[4])

        if checkTimeIndex == 2 and self.parse_year == False and self.set_day_done == False:
            if cur_unit_month == self.tp.tunit[1]:
                if cur_unit_day <= self.tp.tunit[2]:
                    self.tp.tunit[0] = int(time_arr[0])
        # debug用
        #if checkTimeIndex == 4:
        #    print(checkTimeIndex, self.parse_year, self.parse_month, self.parse_day, self.set_minute_done, cur_unit_hour, cur_unit_minute, self.tp.tunit[3], self.tp.tunit[4], self.parse_morning, self.parse_currelated)

        if checkTimeIndex == 4 and self.parse_year == False and self.parse_month == False and self.parse_day == False and self.set_minute_done == False and self.parse_currelated == False and self.parse_spanrelated == False:
            if cur_unit_hour*100 + cur_unit_minute <= self.tp.tunit[3]*100 + self.tp.tunit[4]:
                logger.debug('Modified Time Unit: [Year, Month, Day]')
                self.tp.tunit[0] = int(time_arr[0])
                self.tp.tunit[1] = int(time_arr[1])  
                self.tp.tunit[2] = int(time_arr[2])
            if self.parse_morning == False and self.tp.tunit[3]*100 + self.tp.tunit[4] <= cur_unit_hour*100 + cur_unit_minute and self.tp.tunit[3] <= 12 and (self.tp.tunit[3]+12)*100 + self.tp.tunit[4] >= cur_unit_hour*100 + cur_unit_minute:
                logger.debug('Modified Time Unit: [Year, Month, Day, Hour]')
                self.tp.tunit[0] = int(time_arr[0])
                self.tp.tunit[1] = int(time_arr[1])  
                self.tp.tunit[2] = int(time_arr[2])
                self.tp.tunit[3] = self.tp.tunit[3] + 12

        
        # 1. 檢查被檢查的時間級別之前，是否沒有更高級的已經確定的時間，如果有，則不進行處理
        for i in range(0, checkTimeIndex):
            if self.tp.tunit[i] != -1:
                return
        # 2. 根據上下文補充時間
        self.checkContextTime(checkTimeIndex)
        # 3. 根據上下文補充時間後再次檢查被檢查的時間級別之前，是否沒有更高級的已經確定的時間，如果有，則不進行傾向處理.
        for i in range(0, checkTimeIndex):
            if self.tp.tunit[i] != -1:
                #print(str(self.tp.tunit[i]))
                return
        # 4. 確認用戶選項
        if not self.normalizer.isPreferFuture:
            return
        # 5. 獲取當前時間，如果識別到的時間小於當前時間，則將其上的所有級別時間設置為當前時間，並且其上一級的時間步長+1
        time_arr = self.normalizer.timeBase.split('-')
        cur = arrow.get(self.normalizer.timeBase, "YYYY-M-D-H-m-s")
        cur_unit = int(time_arr[checkTimeIndex])
        if cur_unit < self.tp.tunit[checkTimeIndex]:
            # for debug
            # print(str(cur_unit)+ "XXX" + str(self.tp.tunit[checkTimeIndex]))
            return
        
        # 自己增加的
        #if self.parse_year == False:
        #    self.tp.tunit[0] = int(time_arr[0])
        #    return
        #if self.parse_month == False:
        #    self.tp.tunit[1] = nint(time_arr[1])
        #    return


        # 準備增加的時間單位是被檢查的時間的上一級，將上一級時間+1
        #print(checkTimeIndex)

        cur = self.addTime(cur, checkTimeIndex - 1)
        time_arr = cur.format("YYYY-M-D-H-m-s").split('-')
        for i in range(0, checkTimeIndex):
            self.tp.tunit[i] = int(time_arr[i])
            # if i == 1:
            #     self.tp.tunit[i] += 1

    def checkContextTime(self, checkTimeIndex):
        """
        根據上下文時間補充時間信息
        :param checkTimeIndex:
        :return:
        """
        for i in range(0, checkTimeIndex):
            if self.tp.tunit[i] == -1 and self.tp_origin.tunit[i] != -1:
                self.tp.tunit[i] = self.tp_origin.tunit[i]
        # 在處理小時這個級別時，如果上文時間是下午的且下文沒有主動聲明小時級別以上的時間，則也把下文時間設為下午
        if self.isFirstTimeSolveContext is True and checkTimeIndex == 3 and self.tp_origin.tunit[
            checkTimeIndex] >= 12 and self.tp.tunit[checkTimeIndex] < 12:
            self.tp.tunit[checkTimeIndex] += 12
        self.isFirstTimeSolveContext = False

    def addTime(self, cur, fore_unit):
        if fore_unit == 0:
            cur = cur.shift(years=1)
        elif fore_unit == 1:
            cur = cur.shift(months=1)
        elif fore_unit == 2:
            cur = cur.shift(days=1)
        elif fore_unit == 3:
            cur = cur.shift(hours=1)
        elif fore_unit == 4:
            cur = cur.shift(minutes=1)
        elif fore_unit == 5:
            cur = cur.shift(seconds=1)
        return cur


