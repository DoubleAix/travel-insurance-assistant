# -*- coding: utf-8 -*-

import pickle
import regex as re
import arrow
import json
import os

from .StringPreHandler import StringPreHandler
from .TimePoint import TimePoint
from .TimeUnit import TimeUnit

import copy
import sys
import imp

from .DebugLogging import logger

# 時間表達式識別的主要工作類
class TimeNormalizer(object):
    def __init__(self, isPreferFuture=True):
        """
        :param isPreferFuture: 假如無辨識某些時間單位，是否傾向往未來的時間
        """
        self.isPreferFuture = isPreferFuture
        self.pattern, self.holi_solar, self.holi_lunar = self.init()

    def init(self):
#原本的                           
#         fpath = os.path.dirname(__file__) + '/resource/reg.pkl'
#         try:
#             with open(fpath, 'rb') as f:
#                 pattern = pickle.load(f)
#         except:
#             with open(os.path.dirname(__file__) + '/resource/regex.txt', 'r', encoding='utf-8') as f:
#                 content = f.read()
#             p = re.compile(str(content))
#             with open(fpath, 'wb') as f:
#                 pickle.dump(p, f)
#             with open(fpath, 'rb') as f:
#                 pattern = pickle.load(f)

#修改後的(原本不支援直接修改regex.txt，測試的時候這樣比較方便)       
        fpath = os.path.dirname(__file__) + '/resource/reg.pkl'
        with open(os.path.dirname(__file__) + '/resource/regex_v2.txt', 'r', encoding='utf-8') as f:
            content = f.read()
            
        p = re.compile(str(content.replace("\n","|")))
        with open(fpath, 'wb') as f:
            pickle.dump(p, f)
        with open(fpath, 'rb') as f:
            pattern = pickle.load(f)
        with open(os.path.dirname(__file__) + '/resource/holi_solar.json', 'r', encoding='utf-8') as f:
            holi_solar = json.load(f)
        with open(os.path.dirname(__file__) + '/resource/holi_lunar.json', 'r', encoding='utf-8') as f:
            holi_lunar = json.load(f)
        return pattern, holi_solar, holi_lunar

    def parse(self, target, timeBase=arrow.now()):
        """
        TimeNormalizer的構造方法，timeBase取默認的系統當前時間
        :param timeBase: 基準時間點
        :param target: 待分析字符串
        :return: 時間單元數組
        """
        self.isTimeSpan = False #在專案裡面不用
        self.invalidSpan = False #在專案裡面不用
        self.timeSpan = '' #在專案裡面不用
        self.target = str(target)
        self.timeBase = arrow.get(timeBase).format('YYYY-M-D-H-m-s')
        self.oldTimeBase = self.timeBase
        
        logger.debug("RawText:" + " [" + self.target + "]")
        self.__preHandling() #字串預處理全部都先放在這邊，主要是有些正規表達不好寫，偷吃步的地方
        self.timeToken = self.__timeEx() #用來組合一開始正規表達式之後的碎片，組合成TOKEN，以字串來說TOKEN是最小的parse單位(timestamp or timedelta)
        dic = {} #在網投專案裡面不用
        res = self.timeToken
        ## 自己新增for網投,舊的不符合需求
        ## TODO:未來可以設定連續兩個時間可以計算timedelta
        ## TODO:要處理空字典問題
        if len(res) == 0:
            #return json.dumps([{'type':'no time pattern','value':'nothing'}])
            return [{'type':'no time pattern','value':'nothing'}]
        results=[]
        for r in res:
            #print("timepoint:"+str(r.tp.tunit))
            tmp={}
            #print(r.isTimeSpan)
            if r.isTimeSpan:
                if r.invalidSpan:
                    pass
                else:
                    tmp['type'] = 'timedelta'
                    tmp['value'] = r.timeSpan
            else:
                tmp['type'] = 'timestamp'
                tmp['value'] = r.time.format("YYYY-MM-DD HH:mm:ss")
            if tmp:
                results.append(tmp)
        logger.debug("ParsedJSON: " + str(results))
        #return json.dumps(results)
        return results
        
# 原本的        
#         if self.isTimeSpan:
#             if self.invalidSpan:
#                 dic['error'] = 'no time pattern could be extracted.'
#             else:
#                 dic['type'] = 'timedelta'
#                 dic['timedelta'] = self.timeSpan
#         else:
#             if len(res) == 0:
#                 dic['error'] = 'no time pattern could be extracted.'
#             elif len(res) == 1:
#                 dic['type'] = 'timestamp'
#                 dic['timestamp'] = res[0].time.format("YYYY-MM-DD HH:mm:ss")
#             else:
#                 dic['type'] = 'timespan'
#                 dic['timespan'] = [res[0].time.format("YYYY-MM-DD HH:mm:ss"), res[1].time.format("YYYY-MM-DD HH:mm:ss")]
#         return json.dumps(dic)

    def __preHandling(self):
        """
        待匹配字符串的清理空白符和語氣助詞以及大寫數字轉化的預處理
        :return:
        """
        self.target = StringPreHandler.fullToHalf(self.target)       
        ##自行新增以投保場景為例(EX:我要去日本玩),理論上最好的方式應該事先把日本抽出來，
        ##已經在regex.txt處理掉了
        #self.target = StringPreHandler.delKeyword(self.target, "去日|去月") 

        ## 我絕不應該把空白鍵去除在網投的應用
        ## self.target = StringPreHandler.delKeyword(self.target, "\\s+")  # 清理空白鍵
        self.target = StringPreHandler.delKeyword(self.target, "[的]+")  # 清理語氣助詞
        self.target = StringPreHandler.numberTranslator(self.target)  # 大寫數字轉換
        
        ## 舉例:處理假如_timeEx會parse 八日回來 為 八日 會影響後續的確認是日期和還是區間
        ## 以專案場景客製化
        ## 所以將八日回來 轉換為 八日當天回來
        ## 其他例子: 總共8日 轉換為 總共8日 (不變)
        self.target = StringPreHandler.handleOnlyDate(self.target)

    def __timeEx(self):
        """

        :param target: 輸入文本字符串
        :param timeBase: 輸入基準時間
        :return: TimeUnit[]時間表達式類型數組
        """
        ## TODO:有空白有逗號先分開
        
        startline = -1
        endline = -1
        rpointer = 0
        temp = []
        match = self.pattern.finditer(self.target)
        ## 增加處理不適當的時間單位在不適當的位置，EX:3點30分5日
        from collections import OrderedDict
        mapping = OrderedDict()

        mapping[0] = '\\d+天'
        mapping[1] = '年'
        mapping[2] = '月'
        mapping[3] = '周|週'
        mapping[4] = '日|號|節'
        mapping[5] = '時|點'
        mapping[6] = '分|半'
        mapping[7] = '秒'

        def position_is_reasonable(m, current_state):
            ## check whether the position of a time unit is reasonable 
            c_state = -1
            for i in range(7):
                if re.search(mapping[i], m):
                    c_state= i
            
            if c_state == -1:    
                return current_state, True
            if current_state < c_state:
                return c_state, True
            else:
                return 0 ,False
                    
        timeslices=[]
        
        current_state = 0
        for m in match:
            timeslices.append(m.group())
            current_state, TrueorFalse = position_is_reasonable(m.group(), current_state)
                       
            startline = m.start()
            
            if startline == endline and TrueorFalse:
                rpointer -= 1
                temp[rpointer] = temp[rpointer] + m.group()
            else:
                temp.append(m.group())
                
            endline = m.end()
            rpointer += 1
        
        
        logger.debug("TimeSlices: " + "["+", ".join(timeslices)+"]")
        logger.debug("TimeTokens: " + "[" + ", ".join(temp)+"]")
        
        ## TODO:記得改成正規表達式的方式，因為是有數字的年月時分秒才算
        ## 假如是timespan的日轉換成天
        counter =0
        for t in temp:
#             if counter == 0:
#                 counter+=1
#                 continue
            
            if any(i in t for i in '當年月時分上中下午晚半點秒')== False:
                temp[counter]=t.replace('日','天')
            counter+=1

        
        res = []
 


        #時間上下文：前一個識別出來的時間會是下一個時間的上下文，用於處理：週六3點到5點這樣的多個時間的識別，第二個5點應該識別到是周六的。
        contextTp = TimePoint()

        for i in range(0, rpointer):
            logger.debug("the TimeToken '{}' is to be parsed".format(temp[i]))
            res.append(TimeUnit(temp[i], self, contextTp))
            logger.debug("TimePoint:" + str(res[i].tp.tunit))
            logger.debug("isTimeSpan:" + str(res[i].isTimeSpan))
            if res[i].isTimeSpan == True:
                logger.debug("invalidSpan:" + str(res[i].invalidSpan))

            #contextTp = copy.deepcopy(res[i].tp)
            contextTp = res[i].tp
        
        res = self.__filterTimeUnit(res)
    
        return res

    def __filterTimeUnit(self, tu_arr):
        """
         過濾timeUnit中無用的識別詞。無用識別詞識別出的時間是1970.01.01 00:00:00（fastTime = 0）
         ：param tu_arr：
         ：返回：
        """
        if (tu_arr is None) or (len(tu_arr) < 1):
            return tu_arr
        res = []
        for tu in tu_arr:
            if tu.time.timestamp != 0:
                res.append(tu)
        return res
