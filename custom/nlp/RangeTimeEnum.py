# -*- coding: utf-8 -*-

# 範圍時間的默認時間點
class RangeTimeEnum(object):
    day_break = 3  # 黎明
    early_morning = 8  # 早
    morning = 10  # 上午
    noon = 12  # 中午、午間
    afternoon = 15  # 下午、午後
    night = 18  # 晚上、傍晚
    lateNight = 20  # 晚、晚間
    midNight = 23  # 深夜

