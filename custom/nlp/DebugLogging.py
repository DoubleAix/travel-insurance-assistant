import logging
import sys
logger = logging.getLogger('TimeNLPLogging')
logger.setLevel(logging.DEBUG)
sh = logging.StreamHandler(sys.stdout)
sh.setLevel(logging.INFO)
formatter = logging.Formatter("[%(filename)20s:%(lineno)5s - %(funcName)25s() ] %(message)s")
sh.setFormatter(formatter)
logger.addHandler(sh)