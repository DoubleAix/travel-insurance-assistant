#!venv/bin/python
import sys, os, time, signal
import time

class Daemon(object):
    def __init__(self, name):
        self.name = name
        self.pidfile = "./tmp/{}.pid".format(name.lower().replace(" ","_"))
        
    def record_pid(self,pid):
        if type(pid) == int:
            pid = str(pid)
        with open(self.pidfile,'w+') as f:
            f.write(pid + '\n')
    
    def start(self):
        """Start the daemon."""

        # Check for a pidfile to see if the daemon already runs
        try:
            with open(self.pidfile,'r') as pf:
                pid = int(pf.read().strip())
        except IOError:
            pid = None

        if pid:
            message = "pidfile {0} already exist. " + "{} already running?\n".format(self.name)
            sys.stderr.write(message.format(self.pidfile))
            sys.exit(1)

        # Start the daemon
        sys.stdout.write("Starting {}".format(self.name) + '\n')
        pid = self.run()
        self.record_pid(pid)
        
    def stop(self):
        """Stop the daemon."""

		# Get the pid from the pidfile
        try:
            with open(self.pidfile,'r') as pf:
                pid = int(pf.read().strip())
        except IOError:
            pid = None

        if not pid:
            message = "pidfile {0} does not exist. " + "{} not running?\n".format(self.name)
            sys.stderr.write(message.format(self.pidfile))
            return # not an error in a restart

        # Try killing the daemon process
        sys.stdout.write("Stoping {}".format(self.name) + '\n')
        try:
            while 1:
                os.kill(pid, signal.SIGTERM)
                time.sleep(0.1)
        except OSError as err:
            e = str(err.args)
            if e.find("No such process") > 0:
                if os.path.exists(self.pidfile):
                    os.remove(self.pidfile)
            else:
                print(str(err.args))
                sys.exit(1)
        
    def restart(self):
        """Restart the daemon."""
        self.stop()
        self.start()
        
    def run(self):
        ## return pid number to manage
        pass
    
    def state(self):
        try:
            with open(self.pidfile,'r') as pf:
                pid = int(pf.read().strip())
        except IOError:
            pid = None
            
        if pid:
            return "Active"
        else:
            return "Inactive"