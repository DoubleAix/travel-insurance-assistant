# -*- coding: utf-8 -*-
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from rasa_core.slots import Slot

class ProductSlot(Slot):
    
    def __init__(self, name, 
                 initial_value=None, 
                 value_reset_delay=None, 
                 implemented_products=['旅平險']):
        super(ProductSlot, self).__init__(name, initial_value, value_reset_delay)
        self.implemented_products = implemented_products

                
    def persistence_info(self):
        d = super(ProductSlot, self).persistence_info()
        d["implemented_products"] = self.implemented_products
        return d
    
    def feature_dimensionality(self):
        return 2
    
    def as_feature(self):
        r = [0.0] * self.feature_dimensionality()
        if self.value:
            if self.value in self.implemented_products:
                r[0] = 1.0
            else:
                r[1] = 1.0
        return r