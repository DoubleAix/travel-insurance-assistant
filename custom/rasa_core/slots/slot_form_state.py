# -*- coding: utf-8 -*-
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from rasa_core.slots import Slot

class FormStateSlot(Slot):
    
    def __init__(self, name, 
                 initial_value=None, 
                 value_reset_delay=None, 
                 open_state_name="opening",
                 end_state_name="ending",
                 completed_state_name="completed"):
        super(FormStateSlot, self).__init__(name, initial_value, value_reset_delay)
        self.open_state_name = open_state_name
        self.end_state_name = end_state_name
        self.completed_state_name = completed_state_name
                
    def persistence_info(self):
        d = super(FormStateSlot, self).persistence_info()
        d["open_state_name"] = self.open_state_name
        d["end_state_name"] = self.end_state_name
        d["completed_state_name"] = self.completed_state_name
        return d
    
    def feature_dimensionality(self):
        return 3
    
    def as_feature(self):
        r = [0.0] * self.feature_dimensionality()
        if self.value:
            if self.value == self.open_state_name:
                r[0] = 1.0
            elif self.value == self.end_state_name:
                r[1] = 1.0
            elif self.value == self.completed_state_name:
                r[2] = 1.0
            else:
                pass
        return r
