# -*- coding: utf-8 -*-
# updated along with Rasa Core version 0.11.12
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from rasa_core_sdk.events import SlotSet

from .custom_form_action_extend import IntactFormAction
from .fields import (
    RegionField,
    StartDateField,
    DurationField,
    CoverageField    
)
class ActionRequirementForm(IntactFormAction):
    def name(self):
        return "action_requirement_form"
    
    @staticmethod
    def required_fields():
        return [
            RegionField("place","region"),
            StartDateField("timestamp","start_date"),
            DurationField("timedelta","number","duration"),
            CoverageField("amount","number","coverage")
        ]
    
    @staticmethod
    def slots_mapping():
        return {
        "region":"投保地區",
        "start_date":"開始日期",
        "duration":"投保天數",
        "coverage":"投保金額"
        }
    
    @staticmethod
    def state_name():
        return "requested_slot_requirement"
    
    def submit(self, dispatcher, tracker, domain):
        dispatcher.utter_template(
            "utter_ending_{}".format(self.name()),
            tracker
        )
        dispatcher.utter_template(
            "utter_detail_user_need".format(self.name()),
            tracker
        )        
            
        return [SlotSet(self.state_name(),self.ending_name())]
        
    

