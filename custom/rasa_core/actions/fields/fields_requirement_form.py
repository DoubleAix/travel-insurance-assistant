# -*- coding: utf-8 -*-
# updated along with Rasa Core version 0.11.12
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from rasa_core_sdk.events import SlotSet
from rasa_core_sdk.forms import EntityFormField

from .custom_fields import FuzzyEntityFormField

import pandas as pd
## the path relative to the work directory of excecuting action endpoint server
path = "./data/others/TA_travelregion.xlsx"
mapping_df = pd.read_excel(path).drop(["TYPE"],axis=1)
mapping_dict = dict(zip(mapping_df.PLACENAME, mapping_df.REGION))

class RegionField(EntityFormField):
    def validate(self, value):
        if value in mapping_dict:
            region = mapping_dict[value]
            return region
        else:
            return None


class StartDateField(EntityFormField):
    pass


class DurationField(FuzzyEntityFormField):
    # slot type: float
    def validate(self, value):
        if value is None:
            return value
        if type(value) == str:
            value = value.replace(" days","")
        if type(value) == int:
            value = str(value)
        if int(value) < 1 or int(value) > 60:
            return None
        else:
            return value
    

class CoverageField(FuzzyEntityFormField):
    # slot type: float
    def validate(self, value):
        if value is None:
            return value
        if type(value) == int:
            value = str(value)
        
        # to handle the situation if user only typed just digit number not including "ten thousand"
        if int(value) < 2000:
            value = str(int(value) * 10000)
        return value