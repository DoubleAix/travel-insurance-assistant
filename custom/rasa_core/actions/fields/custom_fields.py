# -*- coding: utf-8 -*-
# updated along with Rasa Core version 0.11.12
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from rasa_core_sdk.forms import EntityFormField
from rasa_core_sdk.events import SlotSet

import logging
import random

logger = logging.getLogger(__name__)

class FuzzyEntityFormField(EntityFormField):
    
    def __init__(self, entity_name, fuzzy_entity_name , slot_name):
        self.entity_name = entity_name
        self.fuzzy_entity_name = fuzzy_entity_name
        self.slot_name = slot_name
    
    def extract(self, tracker):
        events = self.extract_non_fuzzy(tracker)
        if len(events) != 0:
            return events
        else:
            value = next(tracker.get_latest_entity_values(self.fuzzy_entity_name), None)
            validated = self.validate(value)
            if validated is not None:
                return [SlotSet(self.slot_name, validated)]
            return []

    def extract_non_fuzzy(self, tracker):
        value = next(tracker.get_latest_entity_values(self.entity_name), None)
        validated = self.validate(value)
        if validated is not None:
            return [SlotSet(self.slot_name, validated)]
        return []   
    
class MixedFormField(EntityFormField):
       
    def __init__(self, entity_name, slot_name):
        super(MixedFormField, self).__init__(entity_name, slot_name)
    
    def parsefromtext(self, text):
        value = text
        return value
    
    def extract(self, tracker):
        value = next(tracker.get_latest_entity_values(self.entity_name), None)
        validated = self.validate(value)
        if validated is not None:
            return [SlotSet(self.slot_name, validated)]
        
        value = self.parsefromtext(tracker.latest_message.get("text"))
        validated = self.validate(value)
        if validated is not None:
            return [SlotSet(self.slot_name, validated)]
        
        return []
    
    def extract_non_fuzzy(self, tracker):
        events = self.extract(tracker)
        return events