# -*- coding: utf-8 -*-
## updated along with Rasa Core version 0.11.12
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from rasa_core_sdk import Action
from rasa_core_sdk.events import SlotSet

class ActionInsuranceBuy(Action):
   
    def name(self):
    # type: () -> Text
        return "action_insurance_buy"
   
    def run(self, dispatcher, tracker, domain):
    # type: (Dispatcher, DialogueStateTracker, Domain) -> List[Event]
        value = next(tracker.get_latest_entity_values("product"), None)
        if value == None:
            dispatcher.utter_template("utter_not_get_product", tracker)            
        else:
            dispatcher.utter_template("utter_non_implemented_product",tracker)
        return [SlotSet("product", None)]
      
