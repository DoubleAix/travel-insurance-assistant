# -*- coding: utf-8 -*-
# updated along with Rasa Core version 0.11.12
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from rasa_core_sdk import Action, Tracker
from rasa_core_sdk.forms import FormAction, EntityFormField
from rasa_core_sdk.events import SlotSet, Restarted, EventType

import logging
import random
from typing import List, Text


logger = logging.getLogger(__name__)

        
class IntactFormAction(FormAction):
    
    RANDOMIZE = False
    
    def name(self):
    # type: () -> Text
        return self.__class__.__name__
    
    @staticmethod
    def state_name():
        return "requested_slot"
    
    @staticmethod
    def opening_name():
        return "opening"
    
    @staticmethod
    def ending_name():
        return "ending"
    
    @staticmethod
    def completed_name():
        return "completed"
    
    def get_other_slots(self, tracker, dispatcher):
        # type: (Tracker) -> List[EventType]
        requested_slot = tracker.get_slot(self.state_name())

        requested_entity = None
        for f in self.required_fields():
            if f.slot_name == requested_slot:
                requested_entity = getattr(f, 'entity_name', None)

        slot_events = []
        extracted_entities = {requested_entity}

        for f in self.required_fields():
            if isinstance(f, EntityFormField) and \
                not f.slot_name == requested_slot and \
                not f.entity_name in extracted_entities:
                try:
                    slot_events.extend(f.extract_non_fuzzy(tracker))
                except (NameError,  AttributeError) as e:
                    slot_events.extend(f.extract(tracker))
                extracted_entities.add(f.entity_name)
        return slot_events
    
    def get_requested_slot(self, tracker, dispatcher):
        requested_slot = tracker.get_slot(self.state_name())

        required = self.required_fields()

        if self.RANDOMIZE:
            random.shuffle(required)

        if requested_slot is None:
            return []
        else:
            fields = [f for f in required if f.slot_name == requested_slot]
            if len(fields) == 1:
                events = fields[0].extract(tracker)
                if len(events) == 0:
                    ## take a chance to modify
                    dispatcher.utter_template("utter_not_catch_slot", tracker)
                return events
            else:
                logger.debug("unable to extract value "
                             "for requested slot: {}".format(requested_slot))
                return []
    
    def get_opening_slots(self, tracker, dispatcher):
        
        required = self.required_fields()
        slot_events = []
        for f in required:
            try:
                slot_events.extend(f.extract_non_fuzzy(tracker))
            except (NameError, AttributeError) as e:
                slot_events.extend(f.extract(tracker))
        ## if chatbot doesn't catch any slot, it should utter some messages to user.
        if len(slot_events) == 0:
            dispatcher.utter_template("utter_not_catch_slots_opening", tracker)
        return slot_events
    
    def get_slots_if_non_modified_item(self, tracker, dispatcher):
        required = self.required_fields()
        slot_events = []
        requested_slot = tracker.get_slot(self.state_name())
        for f in self.required_fields():
            if isinstance(f, EntityFormField) and f.slot_name == requested_slot:
                try:
                    slot_events.extend(f.extract_non_fuzzy(tracker))
                except (NameError,  AttributeError) as e:
                    slot_events.extend(f.extract(tracker))
        return slot_events
        
    
    def get_modified_slots(self, tracker, dispatcher):
        if sum(1 for item in tracker.get_latest_entity_values('modified_item')) == 0:
            slot_events = self.get_slots_if_non_modified_item(tracker, dispatcher)
            if len(slot_events) == 0:
                dispatcher.utter_template("utter_ask_modify", tracker)
                return []
            else:
                return slot_events
        else:
            required = self.required_fields()
            slot_events = []
            for modified_item in tracker.get_latest_entity_values('modified_item'):
                for f in required:
                    if modified_item == f.slot_name:
                        try:
                            slot_event = f.extract_non_fuzzy(tracker)
                        except (NameError, AttributeError) as e:
                            slot_event = f.extract(tracker)
                        if slot_event:
                            slot_events.extend(slot_event)
                        else:
                            slot_events.extend([SlotSet(modified_item, None)])
            items_list = []
            for e in slot_events:
                #if e.value != None:
                if e['value'] != None:
                    try:
                        items_list.append(self.slots_mapping()[e['name']])
                    except (NameError, AttributeError) as e:
                        items_list.append(e['name'])
            if len(items_list) != 0:
                dispatcher.utter_template("utter_modified", tracker, columns=",".join(items_list))
                                
            return slot_events
        
    def get_ending_slots(self, tracker, dispatcher):
        
        #if tracker.latest_message.intent['name'] == 'affirm':
        if tracker.latest_message.get("intent", {}).get("name") == 'affirm':
            return [SlotSet(self.state_name(), self.completed_name())]
        
        #elif tracker.latest_message.intent['name'] == 'deny':
        if tracker.latest_message.get("intent", {}).get("name") == 'deny':
            dispatcher.utter_template("utter_deny_within_form", tracker)
            return [Restarted()]
        
        else:
            return []
          
    
    def run(self, dispatcher, tracker, domain):
        
        
        requested_slot = tracker.get_slot(self.state_name())
                
        ## handle the situation for user's initial intent       
        if requested_slot is None :
            dispatcher.utter_template(
                    "utter_opening_{}".format(self.name()),
                    tracker,
                    filled_slots=tracker.current_slot_values()
                )
            
            return [SlotSet(self.state_name(), self.opening_name())]
       
        
        ## hanlde the process that user begin to fill in the form
        if requested_slot == self.opening_name():
            events = self.get_opening_slots(tracker, dispatcher)
        
        elif requested_slot == self.ending_name():
            events = self.get_ending_slots(tracker, dispatcher)
            if len(events) != 0:
                return events
            #elif tracker.latest_message.intent['name'] == 'modify':
            elif tracker.latest_message.get("intent", {}).get("name") == 'modify':
                events = self.get_modified_slots(tracker, dispatcher)
                if len(events) == 0:
                    return []
            else:
                return []
       
    
        ## handle the situation if user wants to change the field
        #elif tracker.latest_message.intent['name'] == 'modify':
        elif tracker.latest_message.get("intent", {}).get("name") == 'modify':
            events = self.get_modified_slots(tracker, dispatcher)
            if len(events) == 0:
                return []
        else:
            events = self.get_requested_slot(tracker, dispatcher)
            # the chatbot can catch other slots if only it catched the requested slot
            if len(events) != 0:
                events = events + self.get_other_slots(tracker, dispatcher)
           
        temp_tracker = tracker.copy()
        for e in events:
            #temp_tracker.update(e)
            temp_tracker.slots[e["name"]] = e["value"]

        for field in self.required_fields():
            if self.should_request_slot(temp_tracker, field.slot_name):
                dispatcher.utter_template(
                    "utter_ask_{}".format(field.slot_name),
                    temp_tracker
                )

                events.append(SlotSet(self.state_name(), field.slot_name))
                return events
            
        ## handle the situation for that user has completed the form    
        events_from_submit = self.submit(dispatcher, temp_tracker, domain) or []
        return events + events_from_submit