# -*- coding: utf-8 -*-
# updated along with Rasa Core version 0.11.12
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from rasa_core_sdk import Action
from rasa_core_sdk.events import SlotSet

class ActionPayForm(Action):
   
    def name(self):
    # type: () -> Text
        return "action_pay_form"
    
    def run(self, dispatcher, tracker, domain):
    # type: (Dispatcher, DialogueStateTracker, Domain) -> List[Event]
        dispatcher.utter_template("utter_pay_form",tracker)            
        return []