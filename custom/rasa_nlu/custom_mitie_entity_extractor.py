from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from rasa_nlu.extractors.mitie_entity_extractor import MitieEntityExtractor

class CustomMitieEntityExtractor(MitieEntityExtractor):
    
    name = "custom.rasa_nlu.CustomMitieEntityExtractor"

    requires = ["tokens", "mitie_feature_extractor", "mitie_file","preprocessed_text"]

    def __init__(self,
             component_config=None,  # type: Dict[Text, Any]
             ner=None
             ):
    # type: (...) -> None
        """Construct a new intent classifier using the sklearn framework."""

        super(CustomMitieEntityExtractor, self).__init__(component_config, ner)

    def process(self, message, **kwargs):
    # type: (Message, **Any) -> None
        mitie_feature_extractor = kwargs.get("mitie_feature_extractor")
        if not mitie_feature_extractor:
            raise Exception("Failed to train 'intent_featurizer_mitie'. "
                        "Missing a proper MITIE feature extractor.")

        ents = self.extract_entities(message.get("preprocessed_text"), message.get("tokens"), mitie_feature_extractor)
        extracted = self.add_extractor_name(ents)
        message.set("entities", message.get("entities", []) + extracted, add_to_output=True)
        