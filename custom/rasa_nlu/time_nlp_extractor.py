from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import logging

from rasa_nlu.extractors import EntityExtractor

from ..nlp.TimeNormalizer import TimeNormalizer

logger = logging.getLogger(__name__)

class TimeNLPExtractor(EntityExtractor):
    name = "custom.rasa_nlu.TimeNLPExtractor"
    
    provides = ["entities"]
    
    requires = ["entities","preprocessed_text"]
    
    language_list = ["zh"]
    
    def __init__(self, component_config=None):
        
        super(TimeNLPExtractor, self).__init__(component_config)
        
        self.tn = TimeNormalizer()
    
    
    def timestamp_name(self):
        return "timestamp"
    
    def timedelta_name(self):
        return "timedelta"
    
    def process(self, message, **kwargs):
        # type: (Message, **Any) -> None
        oringinal_entities = message.get("entities", [])[:]
        
        validated = self.validate(message.get('preprocessed_text'))
        if validated != None:
            updated_entities = self.add_extractor_name(validated)
            message.set("entities", oringinal_entities + updated_entities, add_to_output=True)
    
    def validate(self, text):
        try:
            timeunits_list = self.tn.parse(text)
            
        except:
            logger.warn("Can not parse a time format by TimeNormalizer. "
                        "You have to check out TimeNLP source code to fix this problem. ")
            return None
        entities =[]
        for t in timeunits_list:
            if t.get('type') == "no time pattern":
                continue
            elif t.get('type') == "timestamp":
                entity = {'entity': self.timestamp_name(),'value':t['value']}
                entities.append(entity)
            elif t.get('type') == "timedelta":
                entity = {'entity': self.timedelta_name(),'value':t['value']}
                entities.append(entity)
            else:
                continue
        if len(entities) == 0:
            return None
        else:
            return entities
        
                