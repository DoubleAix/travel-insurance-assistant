from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals


from rasa_nlu.extractors import EntityExtractor
#from rasa_nlu.components import Component
#from rasa_nlu.config import RasaNLUModelConfig
#from rasa_nlu.training_data import Message


class OnlyNumberExtractor(EntityExtractor):

    name = "custom.rasa_nlu.OnlyNumberExtractor"
    
    provides = ["intent", "entities"]
    
    requires = ["intent", "entities", "preprocessed_text"]
    
    language_list = ["zh"]
        
    def intent_name(self):
        return "inform"
    
    def entity_name(self):
        return "number"
    
    def process(self, message, **kwargs):
        # type: (Message, **Any) -> None
        
        validated = self.validate(message.get('preprocessed_text'))
        if validated != None:
            intent = {"name": self.intent_name(), "confidence": 1.0}
            message.set("intent", intent, add_to_output=True)
            intent_ranking = [{"name": self.intent_name(), "confidence": 1.0}]
            message.set("intent_ranking", intent_ranking, add_to_output=True)
            entity = {'entity': self.entity_name(),'value':validated}
            entities = self.add_extractor_name([entity])
            message.set("entities", entities, add_to_output=True)
                
    def validate(self, text):
        try:
            text = text.replace("。","")
            number = int(text)
            return number
        except ValueError:
            return None
            
    
