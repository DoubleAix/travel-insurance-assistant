from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from rasa_nlu.components import Component

from ..nlp.StringPreHandler import StringPreHandler

class MessagePreprocessor(Component):
    
    name = "custom.rasa_nlu.MessagePreprocessor"
    
    provides = ['preprocessed_text']
    
    language_list = ["zh"]
    
    def __init__(self, component_config=None):
        
        super(MessagePreprocessor, self).__init__(component_config)
        
        self.sp = StringPreHandler()
    
    def train(self, training_data, config, **kwargs):
        # type: (TrainingData, RasaNLUModelConfig, **Any) -> None
            
        for example in training_data.training_examples:
            text_processed = self.preprocess(example.text)
            example.set("preprocessed_text", text_processed)
            #print(example.get("preprocessed_text"))
    
    def process(self, message, **kwargs):
        # type: (Message, **Any) -> None
        text_processed = self.preprocess_for_train(message.text)
        message.set("preprocessed_text", text_processed, add_to_output = False)

        
    def preprocess(self, text):
        # the string has to be processed in order you need.
        text = self.sp.fullToHalf(text)
        text = self.sp.capitalToSmall(text)
                
        return text
    
    def preprocess_for_train(self, text):
        text = self.preprocess(text)
        text = self.sp.numberTranslator(text)
        
        return text