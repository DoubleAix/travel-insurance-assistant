from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals


from rasa_nlu import utils

from .jieba_tokenizer import JiebaTokenizer

class CustomJiebaTokenizer(JiebaTokenizer):
    
    name = "custom.rasa_nlu.CustomJiebaTokenizer"
    
    provides=['tokens']
    
    requires= ['preprocessed_text']
    
    language_list = ["zh"]
    
    def __init__(self,
                 component_config=None,  # type: Dict[Text, Any]
                 tokenizer=None
                 ):
        # type: (...) -> None
        
        super(CustomJiebaTokenizer, self).__init__(component_config, tokenizer)
        
    
    def train(self, training_data, config, **kwargs):
        # type: (TrainingData, RasaNLUModelConfig, **Any) -> None
            
        for example in training_data.training_examples:
            example.set("tokens", self.tokenize(example.get('preprocessed_text')))
            

    def process(self, message, **kwargs):
        # type: (Message, **Any) -> None
        
        message.set("tokens", self.tokenize(message.get('preprocessed_text')))
        tokenlist = []
        for token in message.get("tokens"):
            tokenlist.append(token.text)
        message.set("tokenslist", tokenlist, add_to_output = False)