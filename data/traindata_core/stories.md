## greet path
* greet
  - utter_greet
  
## goodbye path
* goodbye
  - utter_goodbye
  
## thank path
* thank
  - utter_thank
  
## insurance buy
* insurance_buy{"product" : "沒有開放的商品"} OR insurance_buy{"product" : null}
    - action_insurance_buy
    - slot{"product" : null}
    
## travel insurance - 4 rounds
* insurance_buy{"product" : "旅平險"}
  - action_requirement_form
  - slot{"requested_slot_requirement" : "opening"}
* inform  
  - action_requirement_form
  - slot{"requested_slot_requirement" : "region"}
* inform{"place" : "日本"} OR modify{"place" : "日本"}
  - action_requirement_form
  - slot{"region" : "日本"}
  - slot{"requested_slot_requirement" : "start_date"}
* inform{"timestamp" : "2019-05-30 14:30:00"}
  - action_requirement_form
  - slot{"start_date" : "2019-05-30 14:30:00"}
  - slot{"requested_slot_requirement" : "duration"}
* inform{"number": 4} OR inform{"timedelta": "4"} 
  - action_requirement_form
  - slot{"duration" : "4"}
  - slot{"requested_slot_requirement" : "coverage"}
* inform{"number": 1500} OR inform{"amount": "15000000"}  
  - action_requirement_form
  - slot{"coverage" : "15000000"}
  - slot{"requested_slot_requirement" : "ending"}
> check_ending_requirement_form

## travel insurance - 3 rounds - coverage
* insurance_buy{"product" : "旅平險"}
  - action_requirement_form
  - slot{"requested_slot_requirement" : "opening"}
* inform{"amount": "15000000"}  
  - action_requirement_form
  - slot{"coverage" : "15000000"}
  - slot{"requested_slot_requirement" : "region"}
* inform{"place" : "日本"} OR modify{"place" : "日本"}
  - action_requirement_form
  - slot{"region" : "日本"}
  - slot{"requested_slot_requirement" : "start_date"}
* inform{"timestamp" : "2019-05-30 14:30:00"}
  - action_requirement_form
  - slot{"start_date" : "2019-05-30 14:30:00"}
  - slot{"requested_slot_requirement" : "duration"}
* inform{"number": 4} OR inform{"timedelta": "4"}
  - action_requirement_form
  - slot{"duration" : "4"}
  - slot{"requested_slot_requirement" : "ending"}
> check_ending_requirement_form
  
## travel insurance - 2 rounds - coverage
* insurance_buy{"product" : "旅平險"}
  - action_requirement_form
  - slot{"requested_slot_requirement" : "opening"}
* inform{"amount": "15000000"}  
  - action_requirement_form
  - slot{"coverage" : "15000000"}
  - slot{"requested_slot_requirement" : "region"}
* inform{"place" : "日本"} OR modify{"place" : "日本"}
  - action_requirement_form
  - slot{"region" : "日本"}
  - slot{"requested_slot_requirement" : "start_date"}
* inform{"timestamp" : "2019-05-30 14:30:00","timedelta": "4"}
  - action_requirement_form
  - slot{"start_date" : "2019-05-30 14:30:00"}
  - slot{"duration" : "4"}  
  - slot{"requested_slot_requirement" : "ending"}
> check_ending_requirement_form

## travel insurance - 3 rounds - region
* insurance_buy{"product" : "旅平險"}
  - action_requirement_form
  - slot{"requested_slot_requirement" : "opening"}
* inform{"place" : "日本"} OR modify{"place" : "日本"}
  - action_requirement_form
  - slot{"region" : "日本"}
  - slot{"requested_slot_requirement" : "start_date"}
* inform{"timestamp" : "2019-05-30 14:30:00"}
  - action_requirement_form
  - slot{"start_date" : "2019-05-30 14:30:00"}
  - slot{"requested_slot_requirement" : "duration"}
* inform{"number": 4} OR inform{"timedelta": "4"}
  - action_requirement_form
  - slot{"duration" : "4"}
  - slot{"requested_slot_requirement" : "coverage"}
* inform{"number": 1500} OR inform{"amount": "15000000"}  
  - action_requirement_form
  - slot{"coverage" : "15000000"}  
  - slot{"requested_slot_requirement" : "ending"}
> check_ending_requirement_form
  
## travel insurance - 2 rounds - region
* insurance_buy{"product" : "旅平險"}
  - action_requirement_form
  - slot{"requested_slot_requirement" : "opening"}
* inform{"place" : "日本"} OR modify{"place" : "日本"}
  - action_requirement_form
  - slot{"region" : "日本"}
  - slot{"requested_slot_requirement" : "start_date"}
* inform{"timestamp" : "2019-05-30 14:30:00","timedelta": "4"}
  - action_requirement_form
  - slot{"start_date" : "2019-05-30 14:30:00"}
  - slot{"duration" : "4"}  
  - slot{"requested_slot_requirement" : "coverage"}
* inform{"number": 1500} OR inform{"amount": "15000000"}  
  - action_requirement_form
  - slot{"coverage" : "15000000"}  
  - slot{"requested_slot_requirement" : "ending"}
> check_ending_requirement_form
  
## travel insurance - 0 rounds
* insurance_buy{"product" : "旅平險"}
  - action_requirement_form
  - slot{"requested_slot_requirement" : "opening"}
* inform{"place" : "北海道", "timestamp" : "2019-05-30 14:30:00", "timedelta": "4","amount": "15000000"}
  - action_requirement_form
  - slot{"region" : "日本"}
  - slot{"start_date" : "2019-05-30 14:30:00"}
  - slot{"duration" : "4"}
  - slot{"coverage" : "15000000"}   
  - slot{"requested_slot_requirement" : "ending"}
> check_ending_requirement_form
