## ending requirement form path affirm unexpected path 1
> check_ending_requirement_form
* affirm
  - action_requirement_form
  - slot{"requested_slot_requirement" : "ending"}

## ending requirement form path affirm unexpected path 2
> check_ending_requirement_form
* affirm
  - action_requirement_form
  - slot{"requested_slot_requirement" : "start_date"}



## ending requirement form path deny
> check_ending_requirement_form
* deny
  - action_requirement_form
  
## ending requirement form path affirm
> check_ending_requirement_form
* affirm
  - action_requirement_form
  - slot{"requested_slot_requirement" : "completed"}
  - action_pay_form
  
## ending requirement form path modify 1-1
> check_ending_requirement_form
* modify{"modified_item" : "start_date"}
  - action_requirement_form
  - slot{"start_date": null}
  - slot{"requested_slot_requirement" : "start_date"}
* inform{"timestamp" : "2019-05-30 14:30:00"}
  - action_requirement_form
  - slot{"requested_slot_requirement" : "ending"}
* affirm
  - action_requirement_form
  - slot{"requested_slot_requirement" : "completed"}
  - action_pay_form

## ending requirement form path modify 1-2
> check_ending_requirement_form
* modify{"modified_item" : "duration"}
  - action_requirement_form
  - slot{"duration": null}
  - slot{"requested_slot_requirement" : "duration"}
* inform{"number": 4} OR inform{"timedelta": "4"} 
  - action_requirement_form
  - slot{"requested_slot_requirement" : "ending"}
* affirm
  - action_requirement_form
  - slot{"requested_slot_requirement" : "completed"}
  - action_pay_form


## ending requirement form path modify 2-1
> check_ending_requirement_form
* modify{"modified_item" : "duration", "timedelta": "4" }
  - action_requirement_form
  - slot{"duration": "4"}
  - slot{"requested_slot_requirement" : "ending"}
* affirm
  - action_requirement_form
  - slot{"requested_slot_requirement" : "completed"}
  - action_pay_form

## ending requirement form path modify 2-1
> check_ending_requirement_form
* modify{"modified_item" : "region", "place": "杭州" }
  - action_requirement_form
  - slot{"region": "中國大陸"}
  - slot{"requested_slot_requirement" : "ending"}
* affirm
  - action_requirement_form
  - slot{"requested_slot_requirement" : "completed"}
  - action_pay_form